# Automatic Video Uploader

## System Requirements

NodeJS 4.2.1 or higher is required.  Get it at http://nodejs.org

The following FFMpeg packages must be installed to the system:
- ffmpeg
- ffprobe

They can be obtained here: https://www.ffmpeg.org/download.html

If using a Mac or Linux, download the static binaries, extract them and put them into `/usr/local/bin`

## Setup

Run `npm install` to ensure that all the dependency packages have been installed

Copy or rename `config.js.example` to `config.js` and edit it with the following:
- Local system path where video files are stored
- FTP credentials and path where videos and thumbnails should be uploaded
- Credentials for Drupal site

## Running

Once configuration has been done and dependency packages have been installed (See Setup),
run this command:

`npm start`

## Developer Notes

* Code in the `src` directory
* Run the babel script seen in package.json to build code.
* Execute code out of the `build` directory

For good reference material (Drupal web services) go here:
https://www.drupal.org/node/1354202

This app assumes an OAuth2 interface between the server and the client.
This means that the `oauth2` Drupal module must be installed.  Some amount
of configuration is required on the Drupal end to make this work with `services`.

'use strict';

module.exports = {
  local_path: '/path/to/local/video/files',
  connector: 'FamilyCCConnector',
  email_reports: {
    sender: 'Video Uploader <me@example.com>',
    recipients: ['you@me.com']
  },
  media_server: {
    server: 'your.ftp.server',
    port: 21,
    username: '',
    password: '',
    videos_path: '/streaming',
    thumbnails_path: '/streaming/stills'
  },
  oauth: {
    url_base: 'https://your.domain.here',
    token_endpoint: '/oauth/token',
    client_id: '',
    client_secret: '',
    username: '',
    password: ''
  }
};
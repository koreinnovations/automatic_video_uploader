'use strict';

var _logMessage = require('../lib/logMessage');

var _logMessage2 = _interopRequireDefault(_logMessage);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const _ = require('underscore');
const sprintf = require('sprintf');
const moment = require('moment');
const path = require('path');
const S = require('string');
const ffmpeg = require('fluent-ffmpeg');
const fs = require('fs');
const Q = require('q');

// Taxonomy term IDs for Video Format
const WIDESCREEN = 42;
const STANDARD = 41;

const ConnectorBase = require('./ConnectorBase');
const FileMetaData = require('../lib/FileMetaData');

module.exports = {

  EnsureProperFileEncoding(file_path) {
    let parsed = path.parse(file_path);

    var defer = Q.defer();

    if (parsed.ext.toLowerCase() == '.mpg' || parsed.ext.toLowerCase() == '.mov') {
      (0, _logMessage2.default)(file_path, 'is in mpg or mov format.  Encoding to mp4...');
      let new_path = parsed.dir + '/' + parsed.name + '.mp4';

      var command = ffmpeg(file_path).outputOptions(['-vcodec h264', '-b 3500k', '-strict -2', '-vf yadif=0:-1:0'])
      //.input(full_path)
      .audioCodec('aac');

      command.on('end', function () {
        (0, _logMessage2.default)('Finished with output file', parsed.name, '.mp4');
        fs.renameSync(file_path, parsed.dir + '/originals/' + parsed.name + parsed.ext);
        defer.resolve(new_path);
      });

      command.save(new_path);
    } else {
      defer.resolve(file_path);
    }

    return defer.promise;
  },

  CheckFileNameValidity(filename) {
    // 8 digits (date as YYYYMMDD), followed by 0 or more whitespace characters, followed by a dash,
    // followed by 0 or more whitespace characters, followed by 1 or more characters (speaker name),
    // followed by 0 or more whitespace characters, followed by 1 or more numbers (shoot number),
    // followed by 0 or more whitespace characters followed by a dash
    let name_regex = /^(\d{8})\s*\-\s*(\w+[ \-]\w+)\s*\-\s*((Program-)?(\d+)\s*\-)?(.+)/;

    let result = filename.match(name_regex);

    if (!result) {
      //if (!date_str || date_str.length == 0 || !speaker || speaker.length == 0 || !title || title.length == 0) {
      throw new Error("Filename is not in the proper format to be parsed for FCC.  Please use 'YYYYMMDD - Speaker - Title'");
    }

    // logMessage(result);
    return result;
  },

  GetMetadataFromFile(file_path) {
    let parsed = path.parse(file_path);
    let regex_matches = this.CheckFileNameValidity(parsed.name);

    let date_str = regex_matches[1] ? S(regex_matches[1]).trim().s : null;
    let speaker = regex_matches[2] ? S(regex_matches[2].replace(/-/g, ' ')).trim().s : null;
    let shoot_number = regex_matches[5] ? S(regex_matches[5]).trim().s : null;

    let title_raw = regex_matches[6] ? S(regex_matches[6]).trim().s : null;
    let title;

    if (title_raw) {
      let trimmed_title = S(title_raw.replace(/(-\d{0,8})$/, '')).trim().s;

      let title_with_series_processed;

      let series_matches = trimmed_title.match(/-((pt|part)\s?(\d+))/);
      if (series_matches) {
        title_with_series_processed = trimmed_title.replace(series_matches[0], ', Part ' + series_matches[3]);
      } else {
        title_with_series_processed = trimmed_title;
      }

      title = title_with_series_processed.replace(/-/g, ' ');
    } else {
      title = null;
    }

    // Get a real date
    let date = moment(date_str, "YYYYMMDD");

    return new FileMetaData(file_path, {
      fullpath: file_path,
      date: date,
      speaker: speaker,
      shoot_number: shoot_number,
      title: title
    });
  },

  FormatAsNode(fileMetaData, thumbnailFID) {
    return {
      title: fileMetaData.getTitle(),
      status: 0,
      type: 'on_demand_media',
      language: "en",
      body: {
        "en": [{
          value: sprintf("by %s", fileMetaData.getSpeaker()),
          format: 1
        }]
      },
      field_streaming_video: {
        "und": [{
          stream_name: fileMetaData.getSanitizedFilename(true),
          stream_path: '',
          audio_stream_name: null,
          audio_stream_path: null,
          has_audio_only: null,
          progressive_download: null
        }]
      },
      field_message_date: {
        "und": [{
          value: {
            date: fileMetaData.getDate().format('MM/DD/YYYY'),
            time: fileMetaData.getDate().format('HH:mm')
          }
        }]
      },
      field_still_shot: {
        "und": [{
          fid: thumbnailFID
        }]
      },
      "taxonomy_vocabulary_2": {
        "und": fileMetaData.getSpeaker()
      },
      taxonomy_vocabulary_6: {
        "und": {
          values: fileMetaData.isWidescreen() ? WIDESCREEN : STANDARD
        }
      },
      field_tv_shoot_number: {
        "en": {
          value: fileMetaData.getShootNumber()
        }
      }
    };
  }
};
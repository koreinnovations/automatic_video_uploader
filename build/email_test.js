'use strict';

const EmailSummary = require('./lib/EmailSummary');
const BatchSummary = require('./lib/BatchSummary');
const FileMetaData = require('./lib/FileMetaData');
const FamilyCCConnector = require('./connectors/FamilyCCConnector');
const config = require('./config');

let base_path = '/Volumes/External HD/Public Access-Completed Episodes/originals/';
let succeeding_filename = base_path + "20160302 - Bill Krause - The Need for Love.mpg";
let failing_filename = base_path + "20160629-Bill Krause-Program-40-Putting Your Trust in the Lord.mpg";

let succeeding_metadata = FamilyCCConnector.GetMetadataFromFile(succeeding_filename);
let failing_metadata = FamilyCCConnector.GetMetadataFromFile(failing_filename);
let summary = new BatchSummary();

summary.startTimer();
summary.endTimer();

summary.addWebsiteSuccess(succeeding_metadata, 172);
summary.addWebsiteFailure(failing_metadata, 'Test error from Noah');
summary.addInvalidFormat('Noah test file.jpg');

let emailSummary = new EmailSummary(config.email_reports.sender, config.email_reports.transport, summary);
emailSummary.send(config.email_reports.recipients);
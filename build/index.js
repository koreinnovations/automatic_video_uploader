'use strict';

// Bring in dependency packages

var _logMessage = require('./lib/logMessage');

var _logMessage2 = _interopRequireDefault(_logMessage);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const Q = require('q');
const moment = require('moment');
const _ = require('underscore');
const FTPClient = require('ftp');
const sprintf = require('sprintf').sprintf;
const fs = require('fs');
const ffmpeg = require('fluent-ffmpeg');
const path = require('path');
const process = require('process');

// Bring in local dependencies
const FileMetaData = require('./lib/FileMetaData');
const OAuth2API = require('./lib/OAuth2API');
const YouTubeAPI = require('./lib/YouTubeAPI');
const YouTubeAuthAPI = require('./lib/YouTubeAuthAPI');
const DrupalAPI = require('./lib/DrupalAPI');
const APIHelper = require('./lib/APIHelper');
const BatchSummary = require('./lib/BatchSummary');
const EmailSummary = require('./lib/EmailSummary');
const YouTubeStore = require('./lib/YouTubeStore');

var batchSummary;

// function to encode file data to base64 encoded string
function base64EncodeFile(file) {
  // read binary data
  var bitmap = fs.readFileSync(file);
  // convert binary data to base64 encoded string
  return new Buffer(bitmap).toString('base64');
}

function getLocalPath(dir, filename) {
  return _paths[dir] + '/' + filename;
}

function getConnector(config) {
  switch (config.connector) {
    case 'FamilyCCConnector':
      const FamilyCCConnector = require('./connectors/FamilyCCConnector');
      return FamilyCCConnector;
      break;
    case 'LivePlayConnector':
      const LivePlayConnector = require('./connectors/LivePlayConnector');
      return LivePlayConnector;
      break;
    default:
      throw 'Unrecognized connector';
  }

  return new Module();
}

/**
 * Identifies available video files in the local video folder.
 * Parses out the date, speaker and title from the filenames.
 * Appends each filename, path and metadata to the fileMetaData
 * global array.
 *
 * This is an async function that returns a promise.
 */
function setupLocalFiles() {
  (0, _logMessage2.default)('setupLocalFiles()');
  var defer = Q.defer();

  // Scan the local path and get a list of files.
  fs.readdir(config.local_path, function (err, files) {
    if (err) {
      throw "Error reading files: " + err.message;
    }

    var filePromises = [];
    var filesAsync = [];

    var processLocalFile = function (fileAsync) {
      (0, _logMessage2.default)(fileAsync.filename);

      var fileDefer = fileAsync.deferred;
      var filename = fileAsync.filename;

      // Use the nodejs filesystem api to determine the file type
      var stats = fs.statSync(config.local_path + '/' + filename);
      // Make sure the file is not a directory or a hidden file
      if (stats.isFile() && filename[0] != '.') {
        // Make sure the file ends in "mp4", "m4a" or "mov".  Case insensitive
        if (filename.match(/\.(mp4|m4v|mpg|mov)$/i)) {

          try {

            // If the file is mpg, convert it to mp4
            Connector.EnsureProperFileEncoding(config.local_path + '/' + filename).then(proper_path => {
              var file = Connector.GetMetadataFromFile(proper_path);

              // Append the file's metadata to our array
              fileMetaData.push(file);

              (0, _logMessage2.default)('resolving promise for', proper_path);
              fileDefer.resolve();
            });
          } catch (ex) {
            (0, _logMessage2.default)('Exception thrown during file parsing', ex.message);
            (0, _logMessage2.default)(ex);
            batchSummary.addInvalidFormat(filename);
            fileDefer.reject();
          }
        } else {
          (0, _logMessage2.default)('Filename is in the wrong format', filename);
          batchSummary.addInvalidFormat(filename);
          fileDefer.reject();
        }
      } else {
        (0, _logMessage2.default)('not a valid file');
        fileDefer.reject();
      }

      return fileDefer.promise;
    };

    var processLocalFiles = function () {
      if (filesAsync.length > 0) {
        var fileAsync = filesAsync.shift();
        processLocalFile(fileAsync).then(processLocalFiles, processLocalFiles);
      } else {
        (0, _logMessage2.default)('All done');
      }
    };

    // Loop through each file
    _.each(files, function (filename) {
      var fileDefer = Q.defer();
      filePromises.push(fileDefer.promise);
      filesAsync.push({ filename: filename, deferred: fileDefer });
    });

    processLocalFiles();

    Q.allSettled(filePromises).then(function () {
      (0, _logMessage2.default)('resolving all promises');
      // Resolve the promise
      defer.resolve();
    });
  });

  return defer.promise;
}

function processOneFile() {
  var file = fileMetaData.shift();
  var fileDefer = Q.defer();

  (0, _logMessage2.default)('processing file ', file.getFilename());

  var processMetaData = () => {
    (0, _logMessage2.default)('processMetaData()');
    var d = Q.defer();

    ffmpeg.ffprobe(file.getFullPath(), function (err, metadata) {
      if (err) {
        (0, _logMessage2.default)(err);
        d.reject(err.message);
      } else {
        file.setVideoMetaData(metadata);
        file.setDuration(metadata.format.duration);
        file.setFilesize(metadata.format.size);

        (0, _logMessage2.default)(metadata);

        var stream = _.findWhere(metadata.streams, { codec_name: 'h264' });
        (0, _logMessage2.default)('stream', stream);
        if (!stream) {
          d.reject("Stream is in invalid format");
        } else {
          (0, _logMessage2.default)('stream is valid');
          file.setDimensions(stream.width, stream.height);
          file.setSampleAspectRatio(stream.sample_aspect_ratio);
          file.setDisplayAspectRatio(stream.display_aspect_ratio);
          d.resolve(file);
        }
      }
    });

    return d.promise;
  };

  var generateThumbnails = () => {
    (0, _logMessage2.default)('generateThumbnails()');
    var d = Q.defer();

    let valid_times = file.getValidTimes();

    if (valid_times.length == 0) {
      d.reject('Video is too short to generate thumbnails');
    } else {

      // Get a version of the filename safer for the web
      // and use this version to generate the thumbnails
      let new_filename = file.getSanitizedFilename();

      (0, _logMessage2.default)('Generating thumbnails at', file.getThumbnailDimensionsString());

      ffmpeg(file.getFullPath()).on('filenames', function (filenames) {
        file.setThumbnails(filenames);
      }).on('end', function () {
        d.resolve(file);
      }).screenshots({
        folder: _paths.temp,
        filename: new_filename + '_%wx%h_%0i.png',
        timestamps: valid_times,
        size: file.getThumbnailDimensionsString()
      });

      (0, _logMessage2.default)('extractImage: ' + file.getTitle());
    }

    return d.promise;
  };

  var uploadToServer = () => {
    var d = Q.defer();

    var thumb = file.getPreferredThumbnail();
    // First upload the chosen thumbnail
    (0, _logMessage2.default)('Uploading', thumb);
    ftpClient.put(getLocalPath('temp', thumb), config.media_server.thumbnails_path + '/' + thumb, thumb_err => {
      if (thumb_err) {
        d.reject(thumb_err.message);
      } else {
        var ftp_destination_path = config.media_server.videos_path + '/' + file.getSanitizedFilename(true);
        // Then upload the video itself
        (0, _logMessage2.default)('Uploading', file.getFullPath());
        ftpClient.put(file.getFullPath(), ftp_destination_path, err => {
          if (err) {
            d.reject(err.message);
          } else {
            d.resolve(file);
          }
        });
      }
    });

    (0, _logMessage2.default)('uploadToServer: ' + file.getTitle());

    return d.promise;
  };

  var addToDrupal = () => {
    var d = Q.defer();
    var startTime = moment().unix();

    (0, _logMessage2.default)('addToDrupal: ' + file.getTitle());

    let thumb = file.getPreferredThumbnail();
    var encoded_thumbnail = base64EncodeFile(getLocalPath('temp', thumb));
    DrupalAPI.uploadFile(encoded_thumbnail, thumb).then(fid => {
      (0, _logMessage2.default)("Attached file", fid);

      var data = Connector.FormatAsNode(file, fid);

      (0, _logMessage2.default)('Uploading node ', data.title);

      DrupalAPI.addMediaNode(data).then(nid => {
        (0, _logMessage2.default)('Posted node', nid);
        batchSummary.addWebsiteSuccess(file, moment().unix() - startTime);
        d.resolve(file);
      }, err => {
        batchSummary.addWebsiteFailure(file, err);
        d.reject(err);
      });
    }, err => d.reject(err));

    return d.promise;
  };

  var uploadToYouTube = () => {
    var d = Q.defer();
    var startTime = moment().unix();

    if (config.youtube && config.youtube.upload) {

      YouTubeAPI.uploadVideo(file).then(() => {
        batchSummary.addWebsiteSuccess(file, moment().unix() - startTime);
        d.resolve(file);
      }, err => {
        batchSummary.addYouTubeFailure(file, err);
        d.reject(err);
      });
    } else {
      d.resolve(file);
    }

    return d.promise;
  };

  var moveFileToDone = () => {
    var d = Q.defer();

    (0, _logMessage2.default)('moveFileToDone: ' + file.getTitle());

    // Move thumbnails into appropriate folder
    _.each(file.getThumbnails(), thumb => {
      fs.rename(getLocalPath('temp', thumb), getLocalPath('thumbnails_done', thumb));
    });

    // Move original video into appropriate folder and give it back its original filename
    fs.rename(file.getFullPath(), getLocalPath('video_done', file.getOriginalFilename()), err => {
      if (err) {
        d.reject(err.message);
      } else {
        d.resolve(file);
      }
    });

    return d.promise;
  };

  // This will be called if there is a failure at any point
  // in the processing chain
  var fail = err => {
    (0, _logMessage2.default)('failing', file.getTitle(), '-', err.message);
    fileDefer.reject(err.message);
  };

  // This will be called at the end of the processing
  // chain for each file
  var succeed = () => {
    (0, _logMessage2.default)('succeeding', file.getTitle());
    fileDefer.resolve();
  };

  // Run through the async sequence for the current file.
  // We use promises to make sure each step completes before
  // the next step starts
  processMetaData(file).then(generateThumbnails, error => {
    (0, _logMessage2.default)('failed to process metadata', error);
    fileDefer.reject(error);
  }).then(uploadToServer, error => {
    (0, _logMessage2.default)('failed to generate thumbnails', error);
    fileDefer.reject(error);
  }).then(addToDrupal, err => {
    (0, _logMessage2.default)('error adding to drupal', file.getTitle(), err.message);
    fileDefer.reject(err.message);
  })
  //.then(uploadToYouTube, (err) => {
  //  logMessage('error uploading to youtube', file.getTitle(), err.message);
  //  fileDefer.reject(err.message);
  //})
  .then(moveFileToDone, err => {
    (0, _logMessage2.default)('error moving file to done', file.getTitle(), err.message);
    fileDefer.reject(err.message);
  }).then(succeed, fail);

  return fileDefer.promise;
}

/**
 * Loops through the list of files and runs them through the following
 * sequence:
 *
 * 1. use ffmpeg to extract metadata (duration, filesize, dimensions)
 * 2. generate thumbnails
 * 3. upload the video and one of the thumbnails to the FTP server
 * 4. add the video to the Drupal site via web services
 * 5. move the processed video file and thumbnails into the local "done" folder
 *
 * This is an async function that returns a promise.
 */
function processEachFile() {
  (0, _logMessage2.default)('processEachFile()');

  // Initialize a promise for the whole function
  var defer = Q.defer();

  ftpClient.on('error', err => {
    (0, _logMessage2.default)('Encountered error connecting to FTP server ' + config.media_server.server + '. Error is ' + err.message);
    defer.reject(err.message);
  });

  // This is the callback that will be fired when the FTP connection
  // has been established
  ftpClient.on('ready', () => {

    // Drill into the video directory on the FTP server
    ftpClient.cwd(config.media_server.videos_path, (err, currentDir) => {
      if (err) {
        defer.reject(err.message);
      } else {

        // Loop through each video file identified from the local system.
        // We process videos synchronously, as they tend to be large and
        // we don't want to have a bunch of FTP operations going at a time.
        var processLoop = () => {
          // Figure out how long (in seconds) the app has been running
          var current_duration = moment().unix() - batchSummary.getStart().unix();

          let time_exceeded = current_duration >= config.max_execution_time_seconds;

          // If we've exceeded the max allowed execution time or
          // exhaused the files in the processing queue, end.
          if (time_exceeded || fileMetaData.length == 0) {

            if (time_exceeded) {
              (0, _logMessage2.default)('Maximum execution time of', config.max_execution_time_seconds, 'seconds exceeded');
            } else if (fileMetaData.length == 0) {
              (0, _logMessage2.default)('No more files left!');
            }

            // Output a summary to the screen of what was done.
            batchSummary.summarize();
            // close the ftp connection
            ftpClient.end();
            // resolve the processEachFile() promise so the app can go on to its next
            // task
            defer.resolve();
          } else {
            (0, _logMessage2.default)('Processing next file');
            processOneFile().then(processLoop);
          }
        };

        // Start the synchronous processing loop
        processLoop();
      }
    });
  });

  // connect to FTP server
  ftpClient.connect({
    host: config.media_server.server,
    port: config.media_server.port || 21,
    user: config.media_server.username,
    password: config.media_server.password
  });

  return defer.promise;
}

function emailReport() {
  var emailSummary = new EmailSummary(config.email_reports.sender, config.email_reports.transport, batchSummary);
  return emailSummary.send(config.email_reports.recipients);
}

function setupDirectories() {
  (0, _logMessage2.default)('setupDirectories()');
  var defer = Q.defer();

  // Make sure each directory exists
  _.each(_paths, dir => {
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
  });
  defer.resolve();

  return defer.promise;
}

function obtainDrupalAccess() {
  return OAuth2API.obtainAccessTokenWithCredentials(config.oauth.username, config.oauth.password);
}

function obtainYouTubeAccess() {
  var defer = Q.defer();
  if (config.youtube && config.youtube.upload) {

    YouTubeStore.addInitialAccessTokenCheckedListener(authenticating => {
      (0, _logMessage2.default)('initial access token checked, value ' + authenticating);
      if (!authenticating) {
        YouTubeAuthAPI.obtainAccessToken();
      }
      defer.resolve();
    });

    YouTubeAuthAPI.init(config);
    YouTubeStore.initFromStorage();
  } else {
    defer.resolve();
  }
  return defer.promise;
}

// Load the configuration file
(0, _logMessage2.default)('Loading config');
const config = require('./config.js');
if (!config) {
  throw "Error: config.js missing or unusable";
}

ffmpeg.setFfmpegPath(config.ffmpeg_path);
ffmpeg.setFfprobePath(config.ffprobe_path);

let _paths = {
  temp: config.local_path + '/temp',
  done: config.local_path + '/done',
  originals: config.local_path + '/originals',
  video_done: config.local_path + '/done/video',
  thumbnails_done: config.local_path + '/done/thumbnails'
};

const Connector = getConnector(config);

// Initialize libraries with parameters from config file
APIHelper.init(config.oauth.url_base);
OAuth2API.init(config);

// Initialize an empty array.  This will be populated with
// metadata for the files in our local video folder
var fileMetaData = [];
batchSummary = new BatchSummary();

// Start a timer that will keep track of how long our process taks
batchSummary.startTimer();

// Initialize a new FTP client
var ftpClient = new FTPClient();

var fail = err => {
  batchSummary.endTimer();
  console.warn(err.message);
  throw err;
};

// Get an access token to the Drupal server, then
// run through the main sequence of program execution.
(0, _logMessage2.default)('Obtaining access token to Drupal site');
obtainDrupalAccess()
//.then(obtainYouTubeAccess, fail)
.then(setupLocalFiles, fail).then(setupDirectories, fail).then(processEachFile, fail).then(() => batchSummary.endTimer()).then(emailReport, fail).finally(() => {
  (0, _logMessage2.default)('automatic video uploader batch done. exiting...');
  process.exit();
});
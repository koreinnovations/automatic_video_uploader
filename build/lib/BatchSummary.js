'use strict';

var _logMessage = require('./logMessage');

var _logMessage2 = _interopRequireDefault(_logMessage);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const moment = require('moment');


module.exports = class BatchSummary {
  constructor() {
    this._website_successes = [];
    this._youtube_successes = [];
    this._website_failures = [];
    this._youtube_failures = [];
    this._invalid_formats = [];
  }

  getStart() {
    return this._start;
  }

  getEnd() {
    return this._end;
  }

  getDuration() {
    return moment.duration(this._end.diff(this._start));
  }

  getWebsiteSuccesses() {
    return this._website_successes;
  }

  getWebsiteFailures() {
    return this._website_failures;
  }

  getYouTubeSuccesses() {
    return this._youtube_successes;
  }

  getYouTubeFailures() {
    return this._youtube_failures;
  }

  getInvalidFormats() {
    return this._invalid_formats;
  }

  startTimer() {
    this._start = moment();
  }

  endTimer() {
    this._end = moment();
  }

  addWebsiteSuccess(fileMetaData, processingTimeSeconds) {
    (0, _logMessage2.default)(fileMetaData.getFilename() + ' success');
    this._website_successes.push({ fileMetaData: fileMetaData, processingTimeSeconds: processingTimeSeconds });
  }

  addYouTubeSuccess(fileMetaData, processingTimeSeconds) {
    (0, _logMessage2.default)(fileMetaData.getFilename() + ' success');
    this._youtube_successes.push({ fileMetaData: fileMetaData, processingTimeSeconds: processingTimeSeconds });
  }

  addWebsiteFailure(fileMetaData, message) {
    (0, _logMessage2.default)(fileMetaData.getFilename() + ' ' + message);
    this._website_failures.push({ fileMetaData: fileMetaData, message: message });
  }

  addYouTubeFailure(fileMetaData, message) {
    (0, _logMessage2.default)(fileMetaData.getFilename() + ' ' + message);
    this._youtube_failures.push({ fileMetaData: fileMetaData, message: message });
  }

  addInvalidFormat(filename) {
    this._invalid_formats.push(filename);
  }

  summarize() {
    (0, _logMessage2.default)(this._website_successes.length + ' website successes');
    (0, _logMessage2.default)(this._website_failures.length + ' website failures');

    (0, _logMessage2.default)(this._youtube_successes.length + ' youtube successes');
    (0, _logMessage2.default)(this._youtube_failures.length + ' youtube failures');

    (0, _logMessage2.default)(this._invalid_formats.length + ' invalid formats');
  }

};
'use strict';

const keyMirror = require('keymirror');

module.exports = {

  ActionTypes: keyMirror({
    VALID_ACCESS_TOKEN: null,
    VALID_YOUTUBE_ACCESS_TOKEN: null,
    CONFIGURATION_INITIALIZED: null
  })

};
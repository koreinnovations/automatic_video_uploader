'use strict';

var _logMessage = require('./logMessage');

var _logMessage2 = _interopRequireDefault(_logMessage);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const Q = require('q');

const BatchSummary = require('./BatchSummary');

const nodemailer = require('nodemailer');
const moment = require('moment');
const sprintf = require('sprintf');
const _ = require('underscore');
const fetch = require('node-fetch');

module.exports = class EmailSummary {
  constructor(sender, transportConfig, summary) {
    this._sender = sender;
    this._transportConfig = transportConfig;
    this._summary = summary;
  }

  getMessageContent() {
    var lines = [];

    let website_successes = this._summary.getWebsiteSuccesses();
    let website_failures = this._summary.getWebsiteFailures();
    let youtube_successes = this._summary.getYouTubeSuccesses();
    let youtube_failures = this._summary.getYouTubeFailures();
    let invalid_formats = this._summary.getInvalidFormats();

    if (website_successes.length == 0 && website_failures.length == 0 && youtube_successes.length == 0 && youtube_failures.length == 0 && invalid_formats.length == 0) {
      // skip sending a report because nothing was done or even tried
      (0, _logMessage2.default)('There are no successes or failures to report.');
      return; // "Daily video upload batch - nothing processed today";
    }

    lines.push(sprintf("Started: %s", this._summary.getStart().format("h:mma")));
    lines.push(sprintf("Ended: %s", this._summary.getEnd().format("h:mma")));
    lines.push(sprintf("Duration: %s", this._summary.getDuration().humanize()));
    lines.push('');
    lines.push(sprintf("Processed successfully (website): %d", website_successes.length));
    lines.push(sprintf("Failed to process (website): %d", website_failures.length));
    lines.push(sprintf("Processed successfully (YouTube): %d", youtube_successes.length));
    lines.push(sprintf("Failed to process (YouTube): %d", youtube_failures.length));
    lines.push(sprintf("Skipped due to invalid format: %d", invalid_formats.length));

    if (website_successes.length > 0) {
      lines.push('');
      lines.push('');
      lines.push('ADDED TO WEBSITE');
      lines.push('---------------');
      _.each(website_successes, data => {
        lines.push(sprintf('- %s (processed in %d seconds)', data.fileMetaData.getFilename(), data.processingTimeSeconds));
      });
    }
    if (website_failures.length > 0) {
      lines.push('');
      lines.push('');
      lines.push('ERRORS ADDING TO WEBSITE');
      lines.push('-----------------');
      _.each(website_failures, data => {
        lines.push(sprintf('- %s (error: %s)', data.fileMetaData.getFilename(), data.message));
      });
    }
    if (youtube_successes.length > 0) {
      lines.push('');
      lines.push('');
      lines.push('ADDED TO YOUTUBE');
      lines.push('---------------');
      _.each(youtube_successes, data => {
        lines.push(sprintf('- %s (processed in %d seconds)', data.fileMetaData.getFilename(), data.processingTimeSeconds));
      });
    }
    if (youtube_failures.length > 0) {
      lines.push('');
      lines.push('');
      lines.push('ERRORS ADDING TO YOUTUBE');
      lines.push('-----------------');
      _.each(youtube_failures, data => {
        lines.push(sprintf('- %s (error: %s', data.fileMetaData.getFilename(), data.message));
      });
    }
    if (invalid_formats.length > 0) {
      lines.push('');
      lines.push('');
      lines.push('INVALID FILE FORMAT OR NAME');
      lines.push('---------------------------');
      _.each(invalid_formats, filename => {
        lines.push(filename);
      });
    }

    return lines.join("\n");
  }

  send(recipientsArray) {
    let defer = Q.defer();

    let message = this.getMessageContent();

    if (message && message.length > 0) {
      (0, _logMessage2.default)('Message contents', message);

      // create reusable transporter object using the default SMTP transport
      var transporter = nodemailer.createTransport(this._transportConfig);

      // setup e-mail data with unicode symbols
      var mailOptions = {
        from: this._sender,
        to: recipientsArray.join(', '), // list of receivers
        subject: 'Automated Video Uploader - Batch Summary ' + moment().format('M/D h:mma'), // Subject line
        text: message, // plaintext body
        debug: true,
        logger: true
      };

      // send mail with defined transport object
      let x = transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
          (0, _logMessage2.default)('Encountered error sending mail', error);
          defer.reject(error);
        } else {
          (0, _logMessage2.default)('Message recipients:', recipientsArray.join(', '));
          (0, _logMessage2.default)('Message sent: ' + info.response);
          defer.resolve();
        }
      });
    } else {
      defer.resolve();
    }

    return defer.promise;
  }
};
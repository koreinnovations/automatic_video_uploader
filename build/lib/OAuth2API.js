'use strict';

const Dispatcher = require('./Dispatcher');
const Constants = require('./Constants');
const Q = require('q');
const param = require('jquery-param');
const fetch = require('node-fetch');
const moment = require('moment');
const FormData = require('form-data');

var OAUTH_CONFIG;
var API_URL;

var _is_authorizing = false;

function OAuth2API() {}

// Initializes the authentication manager singleton based on
// a config object
OAuth2API.init = function (config) {
  API_URL = config.oauth.url_base;
  OAUTH_CONFIG = config.oauth;
};

// Obtains access getToken from the API with username and password
OAuth2API.obtainAccessTokenWithCredentials = function (username, password) {
  var defer = Q.defer();

  var url = API_URL + OAUTH_CONFIG.token_endpoint;
  if (!_is_authorizing) {
    _is_authorizing = true;

    var form = new FormData();
    form.append('grant_type', 'password');
    form.append('client_id', OAUTH_CONFIG.client_id);
    form.append('client_secret', OAUTH_CONFIG.client_secret);
    form.append('username', username);
    form.append('password', password);

    fetch(url, { method: 'POST', body: form }).then(res => {
      if (res.status == 200) {
        return res.json();
      } else {
        throw 'Response code ' + res.status;
      }
    }, error => {
      throw error;
    }).then(responseJSON => {
      if (responseJSON.access_token) {
        Dispatcher.dispatch({
          actionType: Constants.ActionTypes.VALID_ACCESS_TOKEN,
          token: responseJSON
        });
        defer.resolve();
      } else {
        defer.reject();
        throw 'No access code returned';
      }
      _is_authorizing = false;
    }).catch(error => {
      _is_authorizing = false;
      defer.reject(error.message);
      // TODO: handle this error
    });
  }

  return defer.promise;
};

// Obtains a new access getToken from an existing access getToken
OAuth2API.refreshAccessToken = function (_token) {
  var url = API_URL + OAUTH_CONFIG.token_endpoint;

  // This bit here prevents multiple token refresh requests from happening
  // at the same time.  Were we to eliminate this check and request 2 tokens
  // at once from the same app session, one of the token requests would fail.
  //
  // The SessionStore manages a pool of promises that are waiting on a valid
  // access token.  Any time an authenticated API request is triggered, it
  // goes into the pool until a valid token has been received.
  // There may be many simultaneous calls to this refresh access token function
  // but only one will run at a time.
  //
  // Using the Flux pattern, we dispatch a message once we've received a valid
  // token and this, in turn, is picked up by SessionStore, which then resolves
  // the promises pool and allows the API requests to proceed.
  if (!_is_authorizing) {
    _is_authorizing = true;

    var body = {
      client_id: OAUTH_CONFIG.client_id,
      client_secret: OAUTH_CONFIG.client_secret,
      refresh_token: _token.refresh_token,
      grant_type: 'refresh_token'
    };
    fetch(url, { method: 'POST', body: param(body) }).then(res => {
      if (res.status == 200) {
        return res.json();
      } else {
        throw 'Response code ' + res.status;
      }
    }, error => {
      throw error;
    }).then(responseJSON => {
      if (responseJSON.access_token) {
        Dispatcher.dispatch({
          actionType: Constants.ActionTypes.VALID_ACCESS_TOKEN,
          token: responseJSON
        });
      } else {
        throw 'No access code returned';
      }
      _is_authorizing = false;
    }).catch(error => {
      _is_authorizing = false;
      // TODO: handle error
    });
  }
};

module.exports = OAuth2API;
'use strict';

const moment = require('moment');
const assign = require('object-assign');

/**
 * Sample Token Payload
 * {
 *   access_token: '31klshklafsbl208130yhlf',
 *   expires_in: 3600,
 *   refresh_token: 'avxclhkvdsalhk3ry80dfhldsfhl',
 * }
 */

// This "class" is a wrapper to the token object that we get from
// the OAuth2 server.  This wrapper has some helpful methods

function OAuth2Token(token) {
  var obtained = token != null ? moment().unix() : 0;

  var self = assign({}, token);

  self.isExpired = function () {
    var seconds_to_expiration = obtained + token.expires_in - moment().unix();
    // 10 minute buffer for access getToken
    return seconds_to_expiration <= 600;
  };

  self.isRefreshable = function () {
    return self.isPresent() && token.refresh_token;
  };

  self.isPresent = function () {
    return token != null && token.access_token;
  };

  self.isValid = function () {
    return self.isPresent() && !self.isExpired();
  };

  self.getAuthorizationHeader = function () {
    return 'OAuth ="' + token.access_token + '"';
  };

  return self;
}

module.exports = OAuth2Token;
'use strict';

var _logMessage = require('./logMessage');

var _logMessage2 = _interopRequireDefault(_logMessage);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const Dispatcher = require('./Dispatcher');
const Constants = require('./Constants');
const OAuth2Token = require('./OAuth2Token');
const OAuth2API = require('./OAuth2API');
const Q = require('q');
const EventEmitter = require('events').EventEmitter;
const assign = require('object-assign');

const SESSION_STARTED_EVENT = 'session_started';
const SESSION_ENDED_EVENT = 'session_ended';

var _current_user_id;
var _token = new OAuth2Token();

var deferredRequestPool = [];

var SessionStore = assign({}, EventEmitter.prototype, {

  emitSessionStarted: function () {
    this.emit(SESSION_STARTED_EVENT);
  },
  emitSessionEnded: function () {
    this.emit(SESSION_ENDED_EVENT);
  },

  addSessionStartedListener: function (callback) {
    this.on(SESSION_STARTED_EVENT, callback);
  },

  removeSessionStartedListener: function (callback) {
    this.removeListener(SESSION_STARTED_EVENT, callback);
  },

  /**
   * @param {function} callback
   */
  addSessionEndedListener: function (callback) {
    this.on(SESSION_ENDED_EVENT, callback);
  },

  removeSessionEndedListener: function (callback) {
    this.removeListener(SESSION_ENDED_EVENT, callback);
  },
  hasValidAccessToken: function () {
    return _token && _token.isValid();
  },
  requestAccessTokenAsync: function () {
    var defer = Q.defer();

    // If an access getToken is not present...
    if (!_token || !_token.isPresent()) {
      (0, _logMessage2.default)("No access token present");
      defer.reject("No access token present");
    }
    // If the getToken is present but expired...
    else if (_token.isExpired()) {
        (0, _logMessage2.default)('Refreshing access token');
        // Make sure our request gets resolved once the refresh is done
        throwInPool(defer);
        // Call out to the API to get a fresh token
        OAuth2API.refreshAccessToken(_token);
      } else {
        (0, _logMessage2.default)('Access token is good to go');
        defer.resolve(_token);
      }

    return defer.promise;
  },
  getCurrentUserID: function () {
    return _current_user_id;
  }

});

function setToken(token) {
  _token = new OAuth2Token(token);
  return _token;
}

// Add the promise to a pool of deferred requests.
// One we get a valid token (see VALID_ACCESS_TOKEN below),
// we will resolve each promise in this pool, which
// will cause the API requests to be sent to the server
function throwInPool(deferred) {
  deferredRequestPool.push(defer);
}

// This pool is populated via calls to SessionStore.requestAccessTokenAsync(),
// which is used by the APIHelper to make sure we have a valid token
// before we submit API requests.
function resolveItemsInDeferredRequestPool() {
  while (deferredRequestPool.length > 0) {
    var item = deferredRequestPool.shift();
    item.resolve(_token);
  }
}

function rejectItemsInDeferredRequestPool() {
  while (deferredRequestPool.length > 0) {
    var item = deferredRequestPool.shift();
    item.reject('Session ended');
  }
}

SessionStore.dispatchToken = Dispatcher.register(function (payload) {

  switch (payload.actionType) {
    case Constants.ActionTypes.VALID_ACCESS_TOKEN:

      (0, _logMessage2.default)("Received access token");
      (0, _logMessage2.default)(payload.token);

      // Save the token to device storage
      setToken(payload.token);

      // Having received a valid access token, we can now loop through
      // any promises that were waiting for a token and resolve them.
      resolveItemsInDeferredRequestPool();

      SessionStore.emitSessionStarted();
      break;

  }
});

module.exports = SessionStore;
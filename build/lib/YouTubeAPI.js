'use strict';

const Dispatcher = require('./Dispatcher');
const Constants = require('./Constants');
const Q = require('q');
const fs = require('fs');
const moment = require('moment');

const YouTubeStore = require('./YouTubeStore');
const FileMetaData = require('./FileMetaData');

// The following are dependencies listed here: https://www.codementor.io/nodejs/tutorial/uploading-videos-to-youtube-with-nodejs-google-api
// YouTube will handle the YouTube API requests
const Youtube = require("youtube-api");

function YouTubeAPI() {}

YouTubeAPI.uploadVideo = function (file) {
  var defer = Q.defer();

  YouTubeStore.requestAccessTokenAsync().then(() => {

    var youtube_video_data = {
      resource: {
        // Video title and description
        snippet: {
          title: file.getTitle(),
          description: file.getDescription()
        },
        status: {
          privacyStatus: "private"
        }
      },
      // This is for the callback function
      part: "snippet,status",

      // Create the readable stream to upload the video
      media: {
        body: fs.createReadStream(file.getFullPath())
      }
    };

    Youtube.videos.insert(youtube_video_data, (err, data) => {
      if (err) {
        defer.reject(err);
      } else {
        console.dir(data);
        defer.resolve(file);
      }
    });
  });

  return defer.promise;
};

module.exports = YouTubeAPI;
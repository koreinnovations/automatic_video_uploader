'use strict';

const Dispatcher = require('./Dispatcher');
const Constants = require('./Constants');
const YouTubeAuthToken = require('./YouTubeAuthToken');
const YouTubeAuthAPI = require('./YouTubeAuthAPI');
const Q = require('q');
const EventEmitter = require('events').EventEmitter;
const assign = require('object-assign');
const fs = require('fs');
const moment = require('moment');

const INITIAL_ACCESS_TOKEN_CHECKED_EVENT = 'initial_access_token_checked';
const READY = 'ready';

const TOKEN_FILE_PATH = './.youtube';

var _current_user_id;
var _token = new YouTubeAuthToken();

var deferredRequestPool = [];

var YouTubeStore = assign({}, EventEmitter.prototype, {

  emitInitialAccessTokenChecked: function (authenticating) {
    this.emit(INITIAL_ACCESS_TOKEN_CHECKED_EVENT, authenticating);
  },
  addInitialAccessTokenCheckedListener: function (callback) {
    this.on(INITIAL_ACCESS_TOKEN_CHECKED_EVENT, callback);
  },

  removeInitialAccessTokenCheckedListener: function (callback) {
    this.removeListener(INITIAL_ACCESS_TOKEN_CHECKED_EVENT, callback);
  },

  emitReady: function () {
    this.emit(READY);
  },
  addReadyListener: function (callback) {
    this.on(READY, callback);
  },
  removeReadyListener: function (callback) {
    this.removeListener(READY, callback);
  },

  initFromStorage: function () {

    try {
      // This will throw an error if the file doesn't exist
      fs.accessSync(TOKEN_FILE_PATH);
    } catch (ex) {
      YouTubeStore.emitInitialAccessTokenChecked(false);
      return;
    }

    try {
      var contents = fs.readFileSync(TOKEN_FILE_PATH, 'utf8');
      logMessage('Read YT config from file');
      let creds = JSON.parse(contents);
      _token = new YouTubeAuthToken(creds);

      YouTubeAuthAPI.refreshAccessToken(_token);
      YouTubeStore.emitInitialAccessTokenChecked(true);
    } catch (ex) {
      console.warn(ex);
    }
  },

  hasValidAccessToken: function () {
    return _token && _token.isValid();
  },
  requestAccessTokenAsync: function () {
    var defer = Q.defer();

    // If an access getToken is not present...
    if (!_token || !_token.isPresent()) {
      logMessage("No YouTube access token present");
      defer.reject("No YouTube access token present");
    }
    // If the getToken is present but expired...
    else if (_token.isExpired()) {
        logMessage('Refreshing access token');
        // Make sure our request gets resolved once the refresh is done
        throwInPool(defer);
        // Call out to the API to get a fresh token
        YouTubeAuthAPI.refreshAccessToken(_token);
      } else {
        logMessage('YouTube access token is good to go');
        defer.resolve(_token);
      }

    return defer.promise;
  }

});

function setToken(token) {
  token.obtained = moment().unix();

  _token = new YouTubeAuthToken(token);

  fs.writeFile(TOKEN_FILE_PATH, JSON.stringify(token), err => {
    if (err) throw err;
    logMessage('It\'s saved!');
  });

  return _token;
}

// Add the promise to a pool of deferred requests.
// One we get a valid token (see VALID_ACCESS_TOKEN below),
// we will resolve each promise in this pool, which
// will cause the API requests to be sent to the server
function throwInPool(deferred) {
  deferredRequestPool.push(defer);
}

// This pool is populated via calls to YouTubeStore.requestAccessTokenAsync(),
// which is used by the APIHelper to make sure we have a valid token
// before we submit API requests.
function resolveItemsInDeferredRequestPool() {
  while (deferredRequestPool.length > 0) {
    var item = deferredRequestPool.shift();
    item.resolve(_token);
  }
}

function rejectItemsInDeferredRequestPool() {
  while (deferredRequestPool.length > 0) {
    var item = deferredRequestPool.shift();
    item.reject('Session ended');
  }
}

YouTubeStore.dispatchToken = Dispatcher.register(function (payload) {

  switch (payload.actionType) {
    case Constants.ActionTypes.VALID_YOUTUBE_ACCESS_TOKEN:

      logMessage("Received YouTube access token");
      logMessage(payload.token);

      // Save the token to device storage
      setToken(payload.token);

      // Having received a valid access token, we can now loop through
      // any promises that were waiting for a token and resolve them.
      resolveItemsInDeferredRequestPool();

      YouTubeStore.emitReady();
      break;

  }
});

module.exports = YouTubeStore;
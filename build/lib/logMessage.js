'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (message) {
  var date = moment().format('YYYY-MM-DD HH:MM:SS');
  var a = [];
  for (var x in arguments) {
    a.push(arguments[x]);
  }
  console.log(date, '-', a.join(' '));
};

const moment = require('moment');
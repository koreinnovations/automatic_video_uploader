'use strict';

function logMessage(message) {
  var a = [];
  for (var x in arguments) {
    a.push(arguments[x]);
  }
  console.log(a.join(' '));
}

logMessage('abcdefg');

logMessage(1, 2, 3);
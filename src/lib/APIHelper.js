/**
 *
 * NOTE: the APIHelper methods use the promises framework, but not
 * to return data.  They simply use the promises framework to signal
 * to the caller that the request succeeded or failed.
 *
 * Data is sent through dispatched events according to the Flux pattern.
 *
 * The promises here can be used, for example, by loading indicators to know
 * when to disappear or display an error.
 *
 */

const Q = require('q');
const param = require('jquery-param');
const FormData = require('form-data');
const SessionStore = require('./SessionStore');
const fetch = require('node-fetch');
const assign = require('object-assign');

var _url_base;

function _initialFetchResponse(res) {
  if (res.status == 200) {
    return res.json();
  }
  else {
    throw  'Response code ' + res.status;
  }
}

function _initialFetchError(error) {
  throw(error);
}


function APIHelper() {
}

function getApiURL(endpoint) {
  return _url_base + endpoint;
}

APIHelper.init = function (url_base) {
  _url_base = url_base;
};

APIHelper.get = function (endpoint, params) {
  var defer = Q.defer();

  var url = getApiURL(endpoint);
  var body = param(params);

  if (body && body.length > 0) {
    url += '?' + body;
  }

  // Now that we have a valid auth token...
  fetch(url, {method: 'GET'})
    .then(_initialFetchResponse, _initialFetchError)
    .then((responseJSON) => {
      defer.resolve(responseJSON);
    })
    .catch((error) => {
      defer.reject(error);
    });

  return defer.promise;
};

APIHelper.post = function (endpoint, data) {
  var defer = Q.defer();

  var url = getApiURL(endpoint);
  var form = new FormData();
  for (var x in data) {
    form.append(x, data[x]);
  }

  // Now that we have a valid auth token...
  fetch(url, {method: 'POST', body: form})
    .then(_initialFetchResponse, _initialFetchError)
    .then((responseJSON) => {
      defer.resolve(responseJSON);
    })
    .catch((error) => {
      defer.reject(error);
    });

  return defer.promise;
};

APIHelper.authenticatedGet = function (endpoint, params) {
  var defer = Q.defer();

  SessionStore.requestAccessTokenAsync().then(function (token) {
    var headers = {
      'Authorization': token.getAuthorizationHeader()
    };
    var url = getApiURL(endpoint);
    var body = param(params);

    if (body && body.length > 0) {
      url += '?' + body;
    }

    // Now that we have a valid auth token...
    fetch(url, {method: 'GET', headers: headers})
      .then(_initialFetchResponse, _initialFetchError)
      .then((responseJSON) => {
        defer.resolve(responseJSON);
      })
      .catch((error) => {
        defer.reject(error);
      });
  }, function () {
    defer.reject();
  });

  return defer.promise;
};

APIHelper.authenticatedJSONPost = function (endpoint, data) {
  var defer = Q.defer();

  SessionStore.requestAccessTokenAsync().then(function (token) {
    var headers = {
      'Authorization': token.getAuthorizationHeader(),
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    };
    var url = getApiURL(endpoint);

    // Now that we have a valid auth token...
    fetch(url, {method: 'POST', body: JSON.stringify(data), headers: headers})
      .then(_initialFetchResponse, _initialFetchError)
      .then((responseJSON) => {
        defer.resolve(responseJSON);
      })
      .catch((error) => {
        defer.reject(error);
      });

  }, function () {
    defer.reject();
  });

  return defer.promise;
};

APIHelper.authenticatedPost = function (endpoint, data, headers) {
  var defer = Q.defer();

  SessionStore.requestAccessTokenAsync().then(function (token) {
    headers = assign({
      'Authorization': token.getAuthorizationHeader(),
    }, headers);
    var url = getApiURL(endpoint);

    // Now that we have a valid auth token...
    fetch(url, {method: 'POST', body: JSON.stringify(data), headers: headers})
      .then(_initialFetchResponse, _initialFetchError)
      .then((responseJSON) => {
        defer.resolve(responseJSON);
      })
      .catch((error) => {
        defer.reject(error);
      });

  }, function () {
    defer.reject();
  });

  return defer.promise;
};

module.exports = APIHelper;

'use strict';

const moment = require('moment');
import logMessage from './logMessage';

module.exports = class BatchSummary {
  constructor() {
    this._website_successes = [];
    this._youtube_successes = [];
    this._website_failures = [];
    this._youtube_failures = [];
    this._invalid_formats = [];
  }

  getStart() {
    return this._start;
  }

  getEnd() {
    return this._end;
  }

  getDuration() {
    return moment.duration(this._end.diff(this._start));
  }

  getWebsiteSuccesses() {
    return this._website_successes;
  }

  getWebsiteFailures() {
    return this._website_failures;
  }

  getYouTubeSuccesses() {
    return this._youtube_successes;
  }

  getYouTubeFailures() {
    return this._youtube_failures;
  }

  getInvalidFormats() {
    return this._invalid_formats;
  }

  startTimer() {
    this._start = moment();
  }

  endTimer() {
    this._end = moment();
  }

  addWebsiteSuccess(fileMetaData, processingTimeSeconds) {
    logMessage(fileMetaData.getFilename() + ' success');
    this._website_successes.push({fileMetaData: fileMetaData, processingTimeSeconds: processingTimeSeconds});
  }

  addYouTubeSuccess(fileMetaData, processingTimeSeconds) {
    logMessage(fileMetaData.getFilename() + ' success');
    this._youtube_successes.push({fileMetaData: fileMetaData, processingTimeSeconds: processingTimeSeconds});
  }

  addWebsiteFailure(fileMetaData, message) {
    logMessage(fileMetaData.getFilename() + ' ' + message);
    this._website_failures.push({fileMetaData: fileMetaData, message: message});
  }

  addYouTubeFailure(fileMetaData, message) {
    logMessage(fileMetaData.getFilename() + ' ' + message);
    this._youtube_failures.push({fileMetaData: fileMetaData, message: message});
  }

  addInvalidFormat(filename) {
    this._invalid_formats.push(filename);
  }

  summarize() {
    logMessage(this._website_successes.length + ' website successes');
    logMessage(this._website_failures.length + ' website failures');

    logMessage(this._youtube_successes.length + ' youtube successes');
    logMessage(this._youtube_failures.length + ' youtube failures');

    logMessage(this._invalid_formats.length + ' invalid formats');
  }

};

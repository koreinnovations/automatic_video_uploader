'use strict';

const Q = require('q');
const Dispatcher = require('./Dispatcher');
const Constants = require('./Constants');
const APIHelper = require('./APIHelper');

import logMessage from './logMessage';

function DrupalAPI() {

}

DrupalAPI.addMediaNode = function (params) {
  var defer = Q.defer();

  APIHelper.authenticatedJSONPost('/api/v1/node', params)
    .then(function (d) {
      logMessage(d);
      if (d.nid) {
        defer.resolve(d.nid);
      } else {
        defer.reject("There was a problem adding this node.");
      }
    }, function (err) {
      logMessage(err);
      defer.reject('There was a problem adding this node. ' + err);
    });

  return defer.promise;
};

DrupalAPI.uploadFile = function (file_encoded, filename) {
  var defer = Q.defer();

  var params = {
    file: file_encoded,
    filename: filename
  };

  APIHelper.authenticatedJSONPost('/api/v1/file', params)
    .then(function (d) {
      logMessage(d);
      if (d.fid) {
        defer.resolve(d.fid);
      } else {
        defer.reject("There was a problem uploading the thumbnail. A");
      }
    }, function (err) {
      logMessage(err);
      defer.reject('There was a problem uploading the thumbnail. B');
    });

  return defer.promise;
};

DrupalAPI.attachThumbnailToMediaNode = function (nid, thumbnail_encoded) {
  var defer = Q.defer();

  var params = {
    field_name: 'field_still_shot',
    "files[field_still_shot]": thumbnail_encoded
  };

  var headers = {};
  //"Content-type": "multipart/form-data"
  // };

  APIHelper.authenticatedPost('/api/v1/node/' + nid + '/attach_file', params, headers)
    .then(function (d) {
      logMessage(d);
      defer.resolve();
    }, function (err) {
      logMessage(err);
      defer.reject('There was a problem attaching a file to this node.');
    });

  return defer.promise;
}

module.exports = DrupalAPI;

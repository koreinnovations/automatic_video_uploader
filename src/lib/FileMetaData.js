'use strict';

const path = require('path');
const _ = require('underscore');
const sprintf = require('sprintf');

module.exports = class FileMetaData {

  constructor(file_path, options) {
    let parsed = path.parse(file_path);

    this._directory = parsed.dir;
    this._originalFilename = parsed.base;
    this._filename = parsed.base;
    this._name_only = parsed.name;
    this._extension = parsed.ext;

    this._date = options.date;
    this._title = options.title;
    this._speaker = options.speaker;
    this._shoot_number = options.shoot_number;
    this._fullpath = options.fullpath;
  }

  getTitle() {
    return this._title;
  }

  getSpeaker() {
    return this._speaker;
  }

  getDate() {
    return this._date;
  }

  getDescription() {
    return sprintf("Message given on %s by %s", this.getDate().format('M/DD/YYYY'), this.getSpeaker());
  }

  getOriginalFilename() {
    return this._originalFilename;
  }

  getFilename() {
    return this._filename;
  }

  getOriginalFullPath() {
    return this._directory + '/' + this._originalFilename;
  }

  getFullPath() {
    return this._directory + '/' + this._filename;
  }

  // This mutates the _filename property
  getSanitizedFilename(include_extension) {
    let sanitized = this._name_only.replace(/[^0-9A-Za-z\.]/g, '_').replace(/_{2,}/g, '_').toLowerCase();
    if (include_extension) {
      return sanitized + this._extension;
    } else {
      return sanitized;
    }
  }

  getValidTimes() {
    var self = this;
    return _.filter([
      10,
      45,
      100,
      200,
      500,
    ], (time) => (self.getDuration() >= time));
  }

  getShootNumber() {
    return this._shoot_number;
  }

  getDuration() {
    return this._duration;
  }

  setDuration(seconds) {
    this._duration = seconds;
  }

  setVideoMetaData(video_metadata) {
    this._videoMetadata = video_metadata;
  }

  setFilesize(bytes) {
    this._filesize = bytes;
  }
  
  setSampleAspectRatio(sample_aspect_ratio) {
    this._sample_aspect_ratio = sample_aspect_ratio;
  }

  setDisplayAspectRatio(display_aspect_ratio) {
    this._display_aspect_ratio = display_aspect_ratio;
  }

  setDimensions(width, height) {
    this._width = width;
    this._height = height;
  }

  setThumbnails(thumbnails) {
    this._thumbnails = thumbnails;
  }

  getThumbnails() {
    return this._thumbnails;
  }

  getPreferredThumbnail() {
    // Pick the second thumbnail in if it exists.
    // Otherwise pick the first
    if (this._thumbnails.length > 1) {
      return this._thumbnails[1];
    } else {
      return this._thumbnails[0];
    }
  }

  getPreferredThumbnailPath() {
    return this._directory + '/' + this.getPreferredThumbnail();
  }

  getThumbnailDimensionsString() {
    var ratio_split = this._display_aspect_ratio.split(':');
    var width_multiplier = ratio_split[0] / ratio_split[1];
    var width = Math.floor(this._height * width_multiplier);
    return sprintf('%dx%d', width, this._height);
  }

  getDimensionsString() {
    return sprintf('%dx%d', this._width, this._height);
  }

  isWidescreen() {
    return (this._height / this._width == 9 / 16);
  }

  isStandard() {
    return (this._height / this._width == 3 / 4);
  }


};

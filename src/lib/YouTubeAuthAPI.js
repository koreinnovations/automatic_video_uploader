'use strict';

const Dispatcher = require('./Dispatcher');
const Constants = require('./Constants');
const Q = require('q');
const param = require('jquery-param');
const fetch = require('node-fetch');
const moment = require('moment');
const FormData = require('form-data');

const FileMetaData = require('./FileMetaData');

import logMessage from './logMessage';

// The following are dependencies listed here: https://www.codementor.io/nodejs/tutorial/uploading-videos-to-youtube-with-nodejs-google-api
// YouTube will handle the YouTube API requests
const Youtube = require("youtube-api");
// `r-json` will read the JSON file (credentials)
const ReadJson = require("r-json");
// `lien` handles the server requests (localhost:5000)
const Lien = require("lien");
// Logging things
const Logger = require("bug-killer");
// Open in the default browser
const Opn = require("opn");

var OAUTH_CONFIG;

var _oauth;
var _is_authorizing = false;

// Init the lien server
var server = new Lien({
  host: "localhost"
  , port: 5000
});

// Here we're waiting for the OAuth2 redirect containing the auth code
server.page.add("/oauth2callback", function (lien) {
  Logger.log("Trying to get the token using the following code: " + lien.search.code);

  // Get the access token
  _oauth.getToken(lien.search.code, function (err, token) {
    _is_authorizing = false;

    if (err) {
      lien.end(err, 400);
      return Logger.log(err);
    }

    // Set the credentials
    _oauth.setCredentials(token);

    Dispatcher.dispatch({
      actionType: Constants.ActionTypes.VALID_YOUTUBE_ACCESS_TOKEN,
      token: token
    });

  });
});

//// Here we're waiting for the OAuth2 redirect containing the auth code
//server.page.add("/oauth2refreshcallback", function (lien) {
//  Logger.log("Trying to get the token using the following code: " + lien.search.code);
//  _is_authorizing = false;
//
//  console.dir(lien);
//
//  //
//  //// Get the access token
//  //_oauth.getToken(lien.search.code, function (err, tokens) {
//  //  _is_authorizing = false;
//  //
//  //  if (err) {
//  //    lien.end(err, 400);
//  //    return Logger.log(err);
//  //  }
//  //
//  //  Dispatcher.dispatch({
//  //    actionType: Constants.ActionTypes.VALID_YOUTUBE_ACCESS_TOKEN,
//  //    token: token
//  //  });
//  //
//  //  // Set the credentials
//  //  _oauth.setCredentials(token);
//  //
//  //  console.dir(token);
//  //
//  //  defer.resolve();
//  //
//  //  lien.end();
//  //
//  //});
//});

function YouTubeAuthAPI() {

}

// Initializes the authentication manager singleton based on
// a config object
YouTubeAuthAPI.init = function (config) {
  OAUTH_CONFIG = config.youtube;
};

// Obtains access getToken from the API with username and password
YouTubeAuthAPI.obtainAccessToken = function () {
  var defer = Q.defer();

  if (!_is_authorizing) {
    _is_authorizing = true;

    // Authenticate using the credentials
    _oauth = Youtube.authenticate({
      type: "oauth",
      client_id: OAUTH_CONFIG.client_id,
      client_secret: OAUTH_CONFIG.client_secret,
      redirect_url: 'http://localhost:5000/oauth2callback'
    });

    // Open the authentication url in the default browser
    Opn(_oauth.generateAuthUrl({
      access_type: "offline"
      , scope: ["https://www.googleapis.com/auth/youtube.upload"]
    }));




  }

  return defer.promise;
};

// Obtains a new access getToken from an existing access getToken
YouTubeAuthAPI.refreshAccessToken = function (_token) {
  var url = 'https://accounts.google.com/o/oauth2/token';

  // This bit here prevents multiple token refresh requests from happening
  // at the same time.  Were we to eliminate this check and request 2 tokens
  // at once from the same app session, one of the token requests would fail.
  //
  // The SessionStore manages a pool of promises that are waiting on a valid
  // access token.  Any time an authenticated API request is triggered, it
  // goes into the pool until a valid token has been received.
  // There may be many simultaneous calls to this refresh access token function
  // but only one will run at a time.
  //
  // Using the Flux pattern, we dispatch a message once we've received a valid
  // token and this, in turn, is picked up by SessionStore, which then resolves
  // the promises pool and allows the API requests to proceed.
  if (!_is_authorizing) {

    logMessage('fetching refresh token');

    _is_authorizing = true;

    var form = new FormData();
    form.append('client_id', OAUTH_CONFIG.client_id);
    form.append('client_secret', OAUTH_CONFIG.client_secret);
    form.append('refresh_token',_token.access_token );
    form.append('grant_type', 'refresh_token' );

    fetch(url, {method: 'POST', body: form})
      .then((res) => {
        if (res.status == 200) {
          return res.json();
        }
        else {
          throw  'Response code ' + res.status;
        }
      }, (error) => {
        throw(error);
      })
      .then((responseJSON) => {
        if (responseJSON.access_token) {

          //_oauth = Youtube.authenticate({
          //  type: "oauth",
          //  access_token: responseJSON.access_token,
          //  client_id: OAUTH_CONFIG.client_id,
          //  client_secret: OAUTH_CONFIG.client_secret,
          //  //redirect_url: "http://localhost:5000/oauth2callback",
          //});
          _oauth = Youtube.authenticate({
            type: "oauth",
            client_id: OAUTH_CONFIG.client_id,
            client_secret: OAUTH_CONFIG.client_secret,
            redirect_url: 'http://localhost:5000/oauth2callback'
          });

          _oauth.setCredentials(responseJSON);

          console.dir(_oauth);

          //// Set the credentials
          //_oauth.setCredentials(responseJSON);

          Dispatcher.dispatch({
            actionType: Constants.ActionTypes.VALID_YOUTUBE_ACCESS_TOKEN,
            token: responseJSON
          });
        } else {
          throw 'No access code returned';
        }
        _is_authorizing = false;
      })
      .catch((error) => {
        console.warn(error);
        _is_authorizing = false;
        // TODO: handle error
      });
  }
};


module.exports = YouTubeAuthAPI;

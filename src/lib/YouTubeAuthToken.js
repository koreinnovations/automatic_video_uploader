'use strict';

const moment = require('moment');
const assign = require('object-assign');

/**
 * Sample Token Payload
 * {
 *   access_token: 'ya29.bALUqsO2Y-rMVnIaxjlXI9L385l0nnU6p1ZbZn49Q3eluqKW5viw7GbC_j83JMAbxtFc',
 *   token_type: 'Bearer',
 *   expiry_date: 'avxcphkvdsalha3ry90dfh0ps6il',
 * }
 */

// This "class" is a wrapper to the token object that we get from
// the OAuth2 server.  This wrapper has some helpful methods

function YouTubeAuthToken(token) {
  var obtained = (token != null) ? moment.unix(token.obtained) : 0;

  var self = assign({}, token);

  self.isExpired = function () {

    let now = moment().unix();
    var seconds_to_expiration;

    if (token.expiry_date) {
      seconds_to_expiration = (token.expiry_date / 1000) - now;
    } else if (token.expires_in) {
      seconds_to_expiration = (obtained + token.expires_in) - now;
    }
    // 10 minute buffer for access getToken
    return (seconds_to_expiration <= 600);
  };

  self.isRefreshable = function () {
    return (self.isPresent());
  };

  self.isPresent = function () {
    return (token != null && token.access_token);
  };

  self.isValid = function () {
    return (self.isPresent() && !self.isExpired());
  };

  self.getAuthorizationHeader = function () {
    return 'Bearer ' + token.access_token;
  };

  return self;
}

module.exports = YouTubeAuthToken;

const moment = require('moment');

export default function (message) {
  var date = moment().format('YYYY-MM-DD HH:MM:SS');
  var a = [];
  for (var x in arguments) {
    a.push(arguments[x]);
  }
  console.log(date, '-', a.join(' '));
}

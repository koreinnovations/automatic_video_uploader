var assert = require('assert');

const FamilyCCConnector = require('../build/connectors/FamilyCCConnector');

let filenames = [
  {
    filename: "20160302 - Bill Krause - The Need for Love.mpg",
    expected: {
      date: '20160302',
      speaker: 'Bill Krause',
      title: 'The Need for Love',
      shoot: null
    }
  },
  {
    filename: "20160402 - Bill Krause - A Future and A Hope.mpg",
    expected: {
      date: '20160402',
      speaker: 'Bill Krause',
      title: 'A Future and A Hope',
      shoot: null
    }
  },
  {
    filename: "20160413 - Bill Krause - Knowing the Love of God.mpg",
    expected: {
      date: '20160413',
      speaker: 'Bill Krause',
      title: 'Knowing the Love of God',
      shoot: null
    }
  },
  {
    filename: "20160420 - Bill Krause - Jehovah Shalom, Our God of Peace.mpg",
    expected: {
      date: '20160420',
      speaker: 'Bill Krause',
      title: 'Jehovah Shalom, Our God of Peace',
      shoot: null
    }
  },
  {
    filename: "20160427 - Bill Krause - Jehovah Shalom, Our God of Peace, Part 2.mpg",
    expected: {
      date: '20160427',
      speaker: 'Bill Krause',
      title: 'Jehovah Shalom, Our God of Peace, Part 2',
      shoot: null
    }
  },
  {
    filename: "20160504-Bill-Krause - Jehovah Rapha, Our God of Healing.mpg",
    expected: {
      date: '20160504',
      speaker: 'Bill Krause',
      title: 'Jehovah Rapha, Our God of Healing',
      shoot: null
    }
  },
  {
    filename: "20160601-Bill Krause-36-It Comes By Obedience.mpg",
    expected: {
      date: '20160601',
      speaker: 'Bill Krause',
      title: 'It Comes By Obedience',
      shoot: '36'
    }
  },
  {
    filename: "20160607-Bill Krause-37-To Honor or To Dishonor.mpg",
    expected: {
      date: '20160607',
      speaker: 'Bill Krause',
      title: 'To Honor or To Dishonor',
      shoot: '37'
    }
  },
  {
    filename: "20160615-Bill Krause-38-Jehovah Jireh.mpg",
    expected: {
      date: '20160615',
      speaker: 'Bill Krause',
      title: 'Jehovah Jireh',
      shoot: '38'
    }
  },
  {
    filename: "20160622-Bill Krause-Program-39-The Five Defaults of Unforgiveness.mpg",
    expected: {
      date: '20160622',
      speaker: 'Bill Krause',
      title: 'The Five Defaults of Unforgiveness',
      shoot: '39'
    }
  },
  {
    filename: "20160629-Bill Krause-Program-40-Putting Your Trust in the Lord.mpg",
    expected: {
      date: '20160629',
      speaker: 'Bill Krause',
      title: 'Putting Your Trust in the Lord',
      shoot: '40'
    }
  },
  {
    filename: "20160629-Bill Krause-Program-41-Trust Not in Natural Knowledge.mpg",
    expected: {
      date: '20160629',
      speaker: 'Bill Krause',
      title: 'Trust Not in Natural Knowledge',
      shoot: '41'
    }
  },
  {
    filename: "20160720-Bill Krause-Program-42-Be Encouraged.mpg",
    expected: {
      date: '20160720',
      speaker: 'Bill Krause',
      title: 'Be Encouraged',
      shoot: '42'
    }
  },
  {
    filename: "20160810-Bill Krause-Program-45-You Have a Calling on Your Life.mpg",
    expected: {
      date: '20160810',
      speaker: 'Bill Krause',
      title: 'You Have a Calling on Your Life',
      shoot: '45'
    }
  },
  {
    filename: "20160817-Bill Krause-Program-46-Knowing the Love-0802.mpg",
    expected: {
      date: '20160817',
      speaker: 'Bill Krause',
      title: 'Knowing the Love',
      shoot: '46'
    }
  },
  {
    filename: "20160824-Bill Krause-Program-47-Knowing the Love-pt2-0802.mpg",
    expected: {
      date: '20160824',
      speaker: 'Bill Krause',
      title: 'Knowing the Love, Part 2',
      shoot: '47'
    }
  },
  {
    filename: "20160831-Bill Krause-Program-48-Knowing the Love-pt3-0816.mpg",
    expected: {
      date: '20160831',
      speaker: 'Bill Krause',
      title: 'Knowing the Love, Part 3',
      shoot: '48'
    }
  },
  {
    filename: "20160907-Bill Krause-Program-49-Knowing the Love-pt4-0823.mpg",
    expected: {
      date: '20160907',
      speaker: 'Bill Krause',
      title: 'Knowing the Love, Part 4',
      shoot: '49'
    }
  },
  {
    filename: "20160914-Bill Krause-Program-50-Seek-First-0814.mpg",
    expected: {
      date: '20160914',
      speaker: 'Bill Krause',
      title: 'Seek First',
      shoot: '50'
    }
  },
  {
    filename: "20160921-Bill Krause-Program-51-Seek-First-What Shall I Give You-0821.mpg",
    expected: {
      date: '20160921',
      speaker: 'Bill Krause',
      title: 'Seek First What Shall I Give You',
      shoot: '51'
    }
  },
  {
    filename: "20160928-Bill Krause-Program-52-Seek-First-The-Benefits-of-Seeking-First-0828.mpg",
    expected: {
      date: '20160928',
      speaker: 'Bill Krause',
      title: 'Seek First The Benefits of Seeking First',
      shoot: '52'
    }
  },
  {
    filename: "20161005-Bill Krause-Program-53-Knowing-the-Love-Rich-in-Mercy-0830.mpg",
    expected: {
      date: '20161005',
      speaker: 'Bill Krause',
      title: 'Knowing the Love Rich in Mercy',
      shoot: '53'
    }
  },
  {
    filename: "20161005-Bill Krause-Program-53-Knowing-the-Love-Rich-in-Mercy-08302016.mpg",
    expected: {
      date: '20161005',
      speaker: 'Bill Krause',
      title: 'Knowing the Love Rich in Mercy',
      shoot: '53'
    }
  },
  {
    filename: "20161005-Bill Krause-Program-53-Knowing-the-Love-Rich-in-Mercy-083016.mpg",
    expected: {
      date: '20161005',
      speaker: 'Bill Krause',
      title: 'Knowing the Love Rich in Mercy',
      shoot: '53'
    }
  }
];

describe('Matching filenames', function () {


  for (var x in filenames) {

    let test_file = filenames[x];

    describe('File ' + test_file.filename, function () {

      it('Should be a valid filename', function () {
        expect(() => FamilyCCConnector.CheckFileNameValidity(test_file.filename)).to.not.throw(Error);
      });

      describe('Metadata from filename', function () {

        let metadata = FamilyCCConnector.GetMetadataFromFile(test_file.filename);
        let expected_data = test_file.expected;


        it('Should properly parse the date from the filename', function () {
          let formatted_date = metadata.getDate().format('YYYYMMDD');
          expect(formatted_date).to.equal(expected_data.date);
        });

        it('Should properly parse the speaker from the filename', function () {
          expect(metadata.getSpeaker()).to.equal(expected_data.speaker);
        });

        it('Should properly parse the title from the filename', function () {
          expect(metadata.getTitle()).to.equal(expected_data.title);
        });

        it('Should properly parse the shoot number from the filename', function () {
          expect(metadata.getShootNumber()).to.equal(expected_data.shoot);
        });

      });

    });


  }


});